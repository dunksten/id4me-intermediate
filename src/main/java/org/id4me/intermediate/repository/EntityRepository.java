package org.id4me.intermediate.repository;

import org.id4me.intermediate.model.FederationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EntityRepository extends JpaRepository<FederationEntity, Long> {
    FederationEntity getFederationEntityByIssAndSub(String iss, String sub);
}
