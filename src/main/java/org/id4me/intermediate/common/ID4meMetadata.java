package org.id4me.intermediate.common;

import net.minidev.json.JSONAware;
import net.minidev.json.JSONObject;

public class ID4meMetadata implements JSONAware {


    private String tl;

    public ID4meMetadata() {
    }

    public String getTl() {
        return tl;
    }

    public void setTl(String tl) {
        this.tl = tl;
    }

    public JSONObject toJSONObject() {
        JSONObject o = new JSONObject();
        if (this.getTl() != null) {
            o.put("TL", this.getTl().toString());
        }

        return o;
    }

    @Override
    public String toJSONString() {
        return this.toJSONObject().toJSONString();
    }
}
