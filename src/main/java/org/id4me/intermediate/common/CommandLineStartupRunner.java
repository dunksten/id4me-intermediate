package org.id4me.intermediate.common;

import com.nimbusds.openid.connect.sdk.federation.entities.FederationMetadataType;
import org.id4me.intermediate.service.FederationEntityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;


@Component
public class CommandLineStartupRunner implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(CommandLineStartupRunner.class);

    @Autowired
    FederationEntityService federationEntityService;

    @Override
    public void run(String... args) {

        logger.info("Application started with command-line arguments: {} . \n To kill this application, press Ctrl + C.", Arrays.toString(args));

        federationEntityService.saveIntermediate();

        federationEntityService.saveFederationEntity("http://localhost:8090", "http://c2id.localhost/c2id");

        logger.info("Trust anchor and OP saved.");

        String metadata = federationEntityService.getSelfSignedEntityStatement("http://localhost:8090").getClaimsSet().getMetadata(new FederationMetadataType("id4me_auditing_partner")).toJSONString();

        logger.info(federationEntityService.getEntityStatement("http://localhost:8090", "http://c2id.localhost/c2id").getClaimsSet().toJSONString());

        logger.info("Metadata: " + metadata);


    }

}