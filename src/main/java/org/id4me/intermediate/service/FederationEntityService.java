package org.id4me.intermediate.service;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.openid.connect.sdk.federation.entities.*;
import com.nimbusds.openid.connect.sdk.op.OIDCProviderConfigurationRequest;
import com.nimbusds.openid.connect.sdk.op.OIDCProviderMetadata;
import org.id4me.intermediate.common.ID4meMetadata;
import org.id4me.intermediate.model.FederationEntity;
import org.id4me.intermediate.repository.EntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.Collections;

@Service
public class FederationEntityService {

    private static final Logger logger = LoggerFactory.getLogger(FederationEntityService.class);
    @Autowired
    EntityRepository entityRepository;

    public void saveFederationEntity(String iss, String sub) {
        FederationEntity federationEntity = new FederationEntity(iss, sub, getJwkSet(sub), getSelfSignedEntityStatement(iss).getClaimsSet().getJWKSet());
        entityRepository.save(federationEntity);
    }

    public JWKSet getLocalJwkSet() {
        FederationEntity federationEntity = entityRepository.getFederationEntityByIssAndSub("http://localhost:8090", "http://localhost:8090");
        logger.info(federationEntity.getJwkIss().toPublicJWKSet().toJSONObject().toJSONString());
        return federationEntity.getJwkIss().toPublicJWKSet();
    }

    public void saveIntermediate() {
        JWKSet jwkSet = null;
        try {
            RSAKey jwk = new RSAKeyGenerator(2048)
                    .algorithm(JWSAlgorithm.RS256)
                    .keyID("2")
                    .keyUse(KeyUse.SIGNATURE)
                    .generate();
            jwkSet = new JWKSet(jwk);
        } catch (JOSEException e) {
            logger.error(e.getMessage());
        }
        ID4meMetadata id4meMetadata = new ID4meMetadata();
        id4meMetadata.setTl("TL3");

        FederationEntity federationEntity = new FederationEntity("http://localhost:8090", "http://localhost:8090", jwkSet, jwkSet, "http://localhost:8090/federation_api_endpoint", null, id4meMetadata.toJSONString());
        entityRepository.save(federationEntity);
        logger.info("Intermediate saved");
    }

    public EntityStatement getSelfSignedEntityStatement(String iss) {
        FederationEntity federationEntity = entityRepository.getFederationEntityByIssAndSub(iss, iss);

        EntityStatement entityStatement = null;
        EntityStatementClaimsSet claims = new EntityStatementClaimsSet(
                new EntityID(federationEntity.getIss()),
                new EntityID(federationEntity.getSub()),
                federationEntity.getIat(),
                federationEntity.getExp(), federationEntity.getJwkIss().toPublicJWKSet());

        if (federationEntity.getFederationApiEndpoint() != null) {
            try {
                FederationEntityMetadata federationEntityMetadata = new FederationEntityMetadata(new URI(federationEntity.getFederationApiEndpoint()));
                claims.setFederationEntityMetadata(federationEntityMetadata);
                ID4meMetadata id4meMetadata = new ID4meMetadata();
                id4meMetadata.setTl("TL3");

                claims.setMetadata(new FederationMetadataType("id4me_auditing_partner"), id4meMetadata.toJSONObject());
                claims.setAuthorityHints(Collections.singletonList(new EntityID("http://localhost:8080")));
            } catch (URISyntaxException e) {
                logger.error(e.getMessage());
            }
        }
        try {
            entityStatement = EntityStatement.sign(claims, federationEntity.getJwkIss().getKeyByKeyId("2"));
            logger.info("Self-signed requested: " + entityStatement.getClaimsSet().toJSONString());
        } catch (JOSEException e) {
            logger.error(e.getMessage());
        }
        return entityStatement;
    }

    public EntityStatement getEntityStatement(String iss, String sub) {
        FederationEntity issEntity = entityRepository.getFederationEntityByIssAndSub(iss, iss);
        FederationEntity subEntity = entityRepository.getFederationEntityByIssAndSub(iss, sub);

        EntityStatement entityStatement = null;
        EntityStatementClaimsSet claims = new EntityStatementClaimsSet(
                new EntityID(issEntity.getIss()),
                new EntityID(subEntity.getSub()),
                subEntity.getIat(),
                subEntity.getExp(), subEntity.getJwkSub());

        if (subEntity.getFederationApiEndpoint() != null) {
            try {
                FederationEntityMetadata federationEntityMetadata = new FederationEntityMetadata(new URI(subEntity.getFederationApiEndpoint()));
                claims.setFederationEntityMetadata(federationEntityMetadata);
            } catch (URISyntaxException e) {
                logger.error(e.getMessage());
            }
        }
        claims.setAuthorityHints(Collections.singletonList(new EntityID("http://localhost:8080")));
        try {
            entityStatement = EntityStatement.sign(claims, issEntity.getJwkIss().getKeyByKeyId("2"));
            logger.info("Get Entity Statement called: " + entityStatement.getClaimsSet().toJSONString());
        } catch (JOSEException e) {
            logger.error(e.getMessage());
        }
        return entityStatement;
    }


    public JWKSet getJwkSet(String entity) {
        URI jwkSetUri = null;
        JWKSet jwkSet = null;

        //Get the jwksSetUri
        try {
            // The OpenID provider issuer URL
            Issuer issuer = new Issuer(entity);

            // Will resolve the OpenID provider metadata automatically
            OIDCProviderConfigurationRequest request = new OIDCProviderConfigurationRequest(issuer);

            // Make HTTP request
            HTTPRequest httpRequest = request.toHTTPRequest();
            logger.info(httpRequest.getURI().toString());
            HTTPResponse httpResponse = httpRequest.send();

            // Parse OpenID provider metadata
            OIDCProviderMetadata opMetadata = OIDCProviderMetadata.parse(httpResponse.getContentAsJSONObject());

            assert issuer.equals(opMetadata.getIssuer());

            // Print the metadata
            jwkSetUri = opMetadata.getJWKSetURI();
            logger.info("JWKS Uri from " + issuer.getValue() + " get: " + jwkSetUri);
        } catch (IOException | com.nimbusds.oauth2.sdk.ParseException e) {
            logger.error(e.getMessage());
        }

        //Get the JWKSet
        try {

            // Make HTTP request
            assert jwkSetUri != null;
            HTTPRequest httpRequest = new HTTPRequest(HTTPRequest.Method.GET, jwkSetUri.toURL());
            HTTPResponse httpResponse = httpRequest.send();
            // Parse JWK Set
            jwkSet = JWKSet.parse(httpResponse.getContentAsJSONObject());
            logger.info("JWKSet get: " + jwkSet.toString());

        } catch (IOException | com.nimbusds.oauth2.sdk.ParseException | ParseException e) {
            logger.error(e.getMessage());
        }
        return jwkSet;
    }
}
