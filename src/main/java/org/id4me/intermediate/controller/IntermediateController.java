package org.id4me.intermediate.controller;

import com.nimbusds.jwt.SignedJWT;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.openid.connect.sdk.SubjectType;
import com.nimbusds.openid.connect.sdk.op.OIDCProviderMetadata;
import net.minidev.json.JSONObject;
import org.id4me.intermediate.service.FederationEntityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;


@RestController
public class IntermediateController {

    private static final Logger logger = LoggerFactory.getLogger(IntermediateController.class);
    @Autowired
    FederationEntityService federationEntityService;

    @GetMapping(value = "/federation_api_endpoint", produces = "application/jose; charset=UTF-8")
    public ResponseEntity<String> apiResponse(@RequestParam(value = "iss") String iss, @RequestParam(value = "sub") String sub) {
        SignedJWT signedJWT = null;
        logger.info("Fed Api: " + iss + ", " + sub);
        try {
            signedJWT = federationEntityService.getEntityStatement(iss, sub).getSignedStatement();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }


        assert signedJWT != null;
        try {
            logger.info(signedJWT.getJWTClaimsSet().toJSONObject().toJSONString());
        } catch (java.text.ParseException e) {
            logger.error(e.getMessage());
        }

        return ResponseEntity.ok().header("Content-Type", "application/jose; charset=UTF-8").body(signedJWT.serialize());
    }

    @GetMapping(value = "/.well-known/jwks.json", produces = "application/json; charset=UTF-8")
    public ResponseEntity<String> jwkResponse() {
        return ResponseEntity.ok().header("Content-Type", "application/json; charset=UTF-8").body(federationEntityService.getLocalJwkSet().toJSONObject().toJSONString());
    }

    @GetMapping(value = "/.well-known/openid-federation", produces = "application/jose; charset=UTF-8")
    public ResponseEntity<String> federationConfiguration() {

        logger.info(String.valueOf(ResponseEntity.ok().header("Content-Type", "application/jose; charset=UTF-8").body(federationEntityService.getSelfSignedEntityStatement("http://localhost:8090").getSignedStatement().serialize())));
        return ResponseEntity.ok().header("Content-Type", "application/jose; charset=UTF-8").body(federationEntityService.getSelfSignedEntityStatement("http://localhost:8090").getSignedStatement().serialize());
    }

    @GetMapping("/.well-known/openid-configuration")
    public JSONObject oidcProviderMetadata() {
        OIDCProviderMetadata oidcProviderMetadata = null;
        try {
            oidcProviderMetadata = new OIDCProviderMetadata(new Issuer("http://localhost:8090"), Collections.singletonList(SubjectType.PUBLIC), new URI("http://localhost:8090/.well-known/jwks.json"));
        } catch (URISyntaxException e) {
            logger.error(e.getMessage());
        }
        assert oidcProviderMetadata != null;

        return oidcProviderMetadata.toJSONObject();
    }
}
