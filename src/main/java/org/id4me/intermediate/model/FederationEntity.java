package org.id4me.intermediate.model;

import com.nimbusds.jose.jwk.JWKSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import java.util.Calendar;
import java.util.Date;

@Entity
public class FederationEntity {

    private static final Logger logger = LoggerFactory.getLogger(FederationEntity.class);
    @Id
    @GeneratedValue
    private long id;
    private String iss;
    private String sub;
    private Date iat;
    private Date exp;
    @Lob
    private JWKSet jwkSub;
    @Lob
    private JWKSet jwkIss;
    private String federationApiEndpoint;
    private String metadataPolicyJson;
    private String metadataJson;

    public FederationEntity(String iss, String sub, JWKSet jwkSub, JWKSet jwkIss) {
        this.iss = iss;
        this.sub = sub;
        this.iat = new Date();
        this.exp = addTimeToExpire(new Date());
        this.jwkSub = jwkSub;
        this.jwkIss = jwkIss;

    }

    public FederationEntity(String iss, String sub, JWKSet jwkSub, JWKSet jwkIss, String federationApiEndpoint) {
        this.iss = iss;
        this.sub = sub;
        this.iat = new Date();
        this.exp = addTimeToExpire(new Date());
        this.jwkSub = jwkSub;
        this.jwkIss = jwkIss;
        this.federationApiEndpoint = federationApiEndpoint;

    }

    public FederationEntity(String iss, String sub, JWKSet jwkSub, JWKSet jwkIss, String federationApiEndpoint, String metadataPolicyJson, String metadataJson) {
        this.iss = iss;
        this.sub = sub;
        this.iat = new Date();
        this.exp = addTimeToExpire(new Date());
        this.jwkSub = jwkSub;
        this.jwkIss = jwkIss;
        this.metadataPolicyJson = metadataPolicyJson;
        this.federationApiEndpoint = federationApiEndpoint;
        this.metadataJson = metadataJson;
    }

    public FederationEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getIss() {
        return iss;
    }

    public void setIss(String iss) {
        this.iss = iss;
    }

    public Date getIat() {
        return iat;
    }

    public void setIat(Date iat) {
        this.iat = iat;
    }

    public Date getExp() {
        return exp;
    }

    public void setExp(Date exp) {
        this.exp = exp;
    }

    public JWKSet getJwkSub() {
        return jwkSub;
    }

    public void setJwkSub(JWKSet jwkSub) {
        this.jwkSub = jwkSub;
    }

    public JWKSet getJwkIss() {
        return jwkIss;
    }

    public void setJwkIss(JWKSet jwkIss) {
        this.jwkIss = jwkIss;
    }

    public String getFederationApiEndpoint() {
        return federationApiEndpoint;
    }

    public void setFederationApiEndpoint(String federationApiEndpoint) {
        this.federationApiEndpoint = federationApiEndpoint;
    }

    public String getMetadataPolicyJson() {
        return metadataPolicyJson;
    }

    public void setMetadataPolicyJson(String metadataPolicyJson) {
        this.metadataPolicyJson = metadataPolicyJson;
    }

    private Date addTimeToExpire(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, 6);

        return calendar.getTime();
    }
}
